<?php
require_once __DIR__ . '/../config.php';
require_once __DIR__ . '/../includes/Database.php';

class Korisnik 
{
    public $id;
    public $ime;
    public $prezime;
    public $korisnicko_ime;
    public $lozinka;
    public $adresa;
    public $telefon;
    public $email;
    public $slika;

    public static function registracija($ime_prezime, $email, $lozinka, $telefon, $slika)
    {
        //$lozinka = hash('sha512', $lozinka);
        $db = Database::getInstance();

        $db->insert('Korisnik', 
            'INSERT INTO korisnici (ime, prezime, korisnicko_ime, lozinka, adresa, telefon, email) 
            VALUES (:ime, :prezime, :korisnicko_ime, :lozinka, :adresa, :telefon, :email)',
            [
                ':ime' =>$ime,
                ':prezime' => $prezime,
                ':lozinka' => $lozinka,
                ':adresa' => $adresa.
                ':telefon' => $telefon,
                ':email' => $email,
                ':slika' => $slika,
            ]
        );

        return $id = $db->lastInsertId();
    }

    public static function get_korisnik(){
        $db=Database::getInstance();

        $korisnici = $db->select('Korisnik', 
            'SELECT * FROM korisnici');

        return $korisnici;
    }

    public static function proveri($email, $lozinka)
    {
        //$lozinka=hash('sha512', $lozinka);
        $db=Database::getInstance();

        $korisnici = $db->select('Korisnik', 
            'SELECT * FROM korisnici 
            WHERE email LIKE :email AND lozinka LIKE :lozinka',
            [
                ':email'=>$email,
                ':lozinka'=>$lozinka
            ]
        );

        foreach($korisnici as $korisnik){
            return $korisnik;
        }
        return null;
    }

    public static function proveri_mail($email)
    {
        $db=Database::getInstance();

        $korisnici = $db->select('Korisnik', 
            'SELECT email FROM korisnici 
            WHERE email LIKE :email',
            [
                ':email'=>$email,
            ]
        );

        foreach($korisnici as $korisnik){
            return $korisnik;
        }
        return null;
    }

    public static function promeni_lozinku($email, $nova_lozinka){
        $db=Database::getInstance();

        $db->update('Korisnik',
            'UPDATE korisnici
            SET lozinka = :nova_lozinka
            WHERE email = :email',
            [
                ':email'=>$email,
                ':nova_lozinka'=>$nova_lozinka,
            ]
        );
    }

    public static function get_korisnik_by_id($id){
        $db=Database::getInstance();

        $korisnici = $db->select('Korisnik', 
            'SELECT * FROM korisnici
            WHERE id = :id',
            [
                ':id'=>$id
            ]
        );
        foreach($korisnici as $korisnik){
            return $korisnik;
        }
        return null;
    }
}