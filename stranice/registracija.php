<!DOCTYPE html>
<html>
<head>
    <title>Registracija</title>
    <link rel="stylesheet" href="main.css">
</head>
<body>
    <form method="post" action="logika/registrujse.php" enctype="multipart/form-data" id="registracija_forma">
        <input type="text" name="email" placeholder="E-mail adresa"><br>
        <input type="text" name="korisnicko_ime" placeholder="Unesite korisnicko ime"><br>
        <input type="password" name="lozinka" placeholder="Lozinka"><br>
        <input type="password" name="ponovi_lozinku" placeholder="Ponovi lozinku"><br>
        <input type="text" name="ime" placeholder="Unesite ime"><br>
        <input type="text" name="prezime" placeholder="Unesite prezime"><br>
        <input type="text" name="adresa" placeholder="Unesite adresu"><br>
        <input type="text" name="telefon" placeholder="Broj telefona"><br>

        <label for="slika">Izaberite sliku velicine do 2MB</label><br>
        <input type="file" name="slika" id="slika" accept="image/jpeg,image/png,image/gif"><br>
        


        <?php if(isset($_GET['greska_mail'])) : ?>
                <p id="greska_postoji_nalog">Vec je registrovan nalog sa tom email adresom</p>
        <?php endif ?>
        <?php if(isset($_GET['greska_lozinka'])) : ?>
                <p id="greska_ponovoljene_lozinke">Lozinke se ne podudaraju</p>
        <?php endif ?>
        
        <input type="submit" value="Registruj se">
        <hr>
        <a href="prijava.php">Prijavi se</a><br>
        <a href="promenaLozinke.php">Promeni lozinku</a><br>
    </form>
</body>
</html>
 